import React from 'react';

const Cell = props => {
	let CellText = '';
	let CellClasses = ['cell'];
	if(props.hasIcon) CellText = '0';
	if(props.isClicked) CellClasses.push('show');
	return <div onClick={props.showCell} className={CellClasses.join(' ')}>{CellText}</div>;
};

export default Cell;