import React, {Component} from 'react';
import './App.css';
import Cells from "./components/cells/cells";
import ButtonReset from "./components/reset/reset";
import Tries from "./components/tries/tries";

class App extends Component {
	
	state = {
		cells: [],
		counter: 0,
		stop: false
	};
	
	getStartMineSweeper = () => {
		const cells = [];
		const counter = 0;
		const stop = false;
		for (let i = 0; i < 36; i++) {
			let cell = {hasIcon: false, isClicked: false, id: i};
			cells.push(cell);
		}
		cells[Math.floor(Math.random() * 36)].hasIcon = true;
		this.setState({cells, counter, stop});
	};
	
	componentDidMount() {this.getStartMineSweeper()}
	
	showCell = (id) => {
		if (!this.state.stop) {
			const index = this.state.cells.findIndex(p => p.id === id);
			const cells = [...this.state.cells];
			let counter = this.state.counter;
			if(!cells[index].isClicked) {
				counter++;
				cells[index].isClicked = true;
			}
			let stop = this.state.stop;
			if (cells[index].hasIcon) {
				alert('You are DIE!!!');
				stop = true;
			}
			this.setState({cells, counter, stop});
		}
	};
	
	resetGame = () => this.getStartMineSweeper();
	
	render() {
		return (
			<div className="App">
				<Cells cells={this.state.cells}	show={this.showCell}/>
				<Tries quantity={this.state.counter}/>
				<ButtonReset reset={this.resetGame}/>
			</div>
		);
	}
}

export default App;
