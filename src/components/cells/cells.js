import React from 'react';
import Cell from "../../cell/cell";

const Cells = props => {
	return (
		<div className="parent">
			{props.cells.map(cell => {
				return <Cell
					key={cell.id}
					hasIcon={cell.hasIcon}
					isClicked={cell.isClicked}
					showCell={() => props.show(cell.id)}
				/>
			})}
		</div>
	)
};

export default Cells;