import React from 'react';

const Tries = props => {
	return <p className="tries">Tries: {props.quantity}</p>;
};

export default Tries;