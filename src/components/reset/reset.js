import React from 'react';

const ButtonReset = props => {
	return <button onClick={props.reset} className="ButtonReset">Reset game</button>;
};

export default ButtonReset;